import React, { useState } from 'react'

function App() {
  const [data, setData] = useState([{alynva: 42}])

  function updateData() {
    setData(oldData => {
      const newData = [...oldData]
      // const newData = oldData.map(o => ({...o}))
      console.log(oldData[0].alynva)
      newData[0].alynva++
      console.log(oldData[0].alynva)
      return newData
    }) 
  }

  return <>
    <div> React version: {React.version} </div>
    <button onClick={updateData}> Increment data[0] </button>
    <div> {JSON.stringify(data)} </div>
  </>
}

export default App